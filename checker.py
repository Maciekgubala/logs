import datetime

import numpy as np
import pandas as pd
import pytz

lista = []

with open('data_provider1.log', 'r') as file_in:
    for line in file_in:
        s1 = line.split(" INFO data_provider - handleData: ")
        if len(s1) > 1 and len(s1[1]) > 30:
            e = eval(s1[1])
            e['date'] = s1[0]
            d = e.pop('thermal')
            for key in d:
                e[key] = d[key]

            lista.append(e)

f = open("output.txt", "w")
f.write(str(lista))
f.close()

with open('output.txt') as f:
    data = eval(f.read())

df = pd.DataFrame(data)
pd.set_option('display.max_columns', None)
df.insert(0, 'New_ID', range(1, 1 + len(df)))


print("log info describe check for count raws = column values:")
print(df.describe())
print("")

print("check for nan values")
print(df.isnull().values.any())
print("")

print("check for 0 values")
boolDf = df.isin([0])
print(boolDf)
print("check for 0 values, beside false warning")
df2 = df.drop('warning', axis=1)
x = 0
if x in df2.values:
    print('Element exists in Dataframe')
else:
    print('Element doesnt exists in Dataframe')


print("")

print("check for target_id dominant values and ammount")
print(df.target_id.value_counts())
print("")

print("check for values where date is not equal timestamp")


def xx(d):
    dt = datetime.datetime.strptime(d, "%Y-%m-%d %H:%M:%S.%f")
    str = dt.strftime("%Y-%m-%d")
    return str


def xx2(d):
    dt = datetime.datetime.fromtimestamp(d, tz=pytz.timezone("Europe/Warsaw"))
    str = dt.strftime("%Y-%m-%d")
    return str


df['costam'] = [xx(d) for d in df["date"]]
df['costam2'] = [xx2(d) for d in df["timestamp"]]

x = np.where(df.costam.isin(df.costam2), df.costam2, 'NO_MATCH')
df3 = df.loc[df['costam'].ne(df['costam2'])]

print(df3)
print("")


print("min, max temp check:")
min_check = df.query("min_temp < 17")
print(min_check)
max_check = df.query("max_temp > 33.4")
print(max_check)
print("")


print("temp_array check of def temp difference" )
df3 = df.explode('temp_array')
df4 = df3.explode("temp_array")
df4['temp_diff'] = df4['temp_array'].diff()
df5 = df4.query("temp_diff > 3")
df5.drop_duplicates(subset="New_ID", keep=False, inplace=True)
print(df5)
print("")

print("other columns values dominant check")
print(df.min_temp.value_counts())
print(df.max_temp.value_counts())
print(df.ambient_temp.value_counts())
print(df.pressure.value_counts())
print(df.humidity.value_counts())
