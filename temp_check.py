import numpy as np
import pandas as pd

list = []
# sparsowanie loga to listy słowników wypłaszczająć termal - 1 linia 1 słownik
with open('data_provider1.log', 'r') as file_in:
    for line in file_in:
        s1 = line.split(" INFO data_provider - handleData: ")
        if len(s1) > 1 and len(s1[1]) > 30:
            e = eval(s1[1])
            e['date'] = s1[0]
            d = e.pop('thermal')
            for key in d:
                e[key] = d[key]

            list.append(e)

f = open("output.txt", "w")
f.write(str(list))
f.close()

with open('output.txt') as f:
    data = eval(f.read())
# wczytanie w pandasowy dataframe
df = pd.DataFrame(data)
pd.set_option('display.max_columns', None)
# dodanie kolumny NEW_ID dla lepszej logistyki odnajdywania logów
df.insert(0, 'New_ID', range(1, 1 + len(df)))

# check dla minimalnych temperatur poniżej zdefiniowaniej niżej wartości
min_check = df.query("min_temp < 16.4")
z = min_check["New_ID"].unique()
print("test minimalnej temp wskazał wpisy id:")
print(z)
print("")

# check dla maksymalnych temperatur powyżej zdefiniowaniej niżej wartości:
max_check = df.query("max_temp > 34")
y = max_check["New_ID"].unique()
print("test max temp wskazał wpisy id:")
print(y)
print("")

pd.options.mode.chained_assignment = None
# rozpakowanie temp_array do rzędów z 1 listą
df3 = df.explode('temp_array')
# rozpakowanie listy do wartości z temperaturą
df4 = df3.explode("temp_array")
# obliczenie różnicy pomiędzy rzędami
df4['temp_diff'] = df4['temp_array'].diff()

# check dla różnicy wwartości temp w temp_array powyżej zdefiniowaniej niżej wartości:
df5 = df4.query("temp_diff > 5")
x = df5["New_ID"].unique()
print("test różnic temp wskazał wpisy id:")
print(x)
print("")

# kopiowanie wyników odchylonych ID z trzech testów i zapisanie do listy
new_arr = np.append(x, y)
wynik = np.append(new_arr, z)
wynik = wynik.tolist()
print("lista odchylonych id z 3 testów:")
print(wynik)
print("")

# filtrowanie pierwotnego dataframa do wyników odchylonych temp w logach po id:
df_filtered = df[df['New_ID'].isin(wynik)]
df_filtered.to_csv("out.txt", index=False)
print("odchylone logi zapisane zostały jako out.txt")
